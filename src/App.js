import {debounce} from 'lodash';
import React, {Component} from 'react';
import YTSearch from 'youtube-api-search';

import SearchBar from './components/SearchBar';
import VideoDetails from './components/VideoDetails';
import VideoList from './components/VideoList';

import './App.css';

const API_KEY = 'AIzaSyC2Q009iTntdBOhh62psOlD-TQvjzjbRq8';
const DEFAULT_TERM = 'surfboards';

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            videos: [],
            selectedVideo: null
        };

        this.searchVideos = debounce(this.searchVideos, 500);
    }

    componentDidMount() {
        this.searchVideos(DEFAULT_TERM);
    }

    searchVideos(term) {
        YTSearch({key: API_KEY, term}, videos => {
            // property shorthand for {videos: videos}
            this.setState({
                videos,
                selectedVideo: videos[0]
            });
        });
    }

    render() {
        return (
            <div className="container">
                <SearchBar onSearchTermChange={term => this.searchVideos(term)} />
                <div className="row">
                    <VideoDetails video={this.state.selectedVideo} />
                    <VideoList
                        onVideoSelect={selectedVideo => this.setState({selectedVideo})}
                        videos={this.state.videos}
                    />
                </div>
            </div>
        );
    }
}

export default App;
