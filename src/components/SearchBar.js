import React, { Component } from 'react';

class SearchBar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            term: ''
        };

        // This binding is necessary to make `this` work in the callback.
        // Or you can also set onChange attribute to the input like so:
        // <input
        //     value={this.state.term}
        //     onChange={(event) => this.onSearchChange(event)}
        // />
        this.onSearchChange = this.onSearchChange.bind(this);
    }

    render() {
        return (
            <div className="search-bar">
                <input
                    value={this.state.term}
                    onChange={this.onSearchChange}
                />
            </div>
        );
    }

    onSearchChange(event) {
        const term = event.target.value;

        this.setState({term});

        this.props.onSearchTermChange(term);
    }
}

export default SearchBar;