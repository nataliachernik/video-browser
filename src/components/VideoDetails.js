import React from "react";

const VideoDetails = ({video}) => {
    if (!video) {
        return <div>Loading...</div>;
    }

    const videoId = video.id.videoId;
    const url = `https://www.youtube.com/embed/${videoId}`;

    return (
        <div className="col-md-8">
            <div className="embed-responsive embed-responsive-16by9">
                <iframe className="embed-responsive-item" src={url} title="youtube video" />
            </div>
            <div className="details">
                <h5>{video.snippet.title}</h5>
                <div>{video.snippet.description}</div>
            </div>
        </div>
    );
};

export default VideoDetails;
